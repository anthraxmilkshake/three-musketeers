//
//  GameBoard.h
//  ThreeMusketeers
//
//  Created by Mark Griffith on 3/28/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import <Foundation/Foundation.h>
@class GameSquare;

@protocol GameBoardDelegate

- (void) boardHasChanged;

@end

@interface GameBoard : NSObject

@property (nonatomic, strong, readonly) GameSquare *cursorSquare;
@property (nonatomic, weak) id<GameBoardDelegate> delegate;

- (char) cursorPieceChar;
- (NSString*)boardStringWithHighlight: (BOOL)shouldHighlight;
- (void) moveCursor:(char)moveChar;
- (GameSquare*)getCursorLeftSquare;
- (GameSquare*)getCursorRightSquare;
- (GameSquare*)getCursorUpSquare;
- (GameSquare*)getCursorDownSquare;
- (BOOL) musketeersLinedUp;
- (BOOL) musketeersCanMove;
- (void)moveCursorSquaretoSquare:(GameSquare*)destination;
- (BOOL) cardinalsMenCanMove;

@end
