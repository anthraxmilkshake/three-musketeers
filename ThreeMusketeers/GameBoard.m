//
//  GameBoard.m
//  ThreeMusketeers
//
//  Created by Mark Griffith on 3/28/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "GameBoard.h"
#import "Musketeer.h"
#import "CardinalMan.h"
#import "GameSquare.h"

@interface GameBoard()

@property (nonatomic, strong) NSArray *boardArrays;
@property (nonatomic, strong, readwrite) GameSquare *cursorSquare;
@property (nonatomic, strong) NSArray *musketeers;
@property (nonatomic, strong) NSMutableArray *cardinalsMen;

@end

@implementation GameBoard

-(NSArray*) boardArrays
{
    if(!_boardArrays) {
        //Setup Board
        NSMutableArray * musketeers = [NSMutableArray arrayWithCapacity:3];
        self.cardinalsMen = [NSMutableArray arrayWithCapacity:22];
        NSMutableArray * rows = [NSMutableArray arrayWithCapacity:5];
        for (int row = 0; row < 5; row++) {
            NSMutableArray * column = [NSMutableArray arrayWithCapacity:5];
            for (int col = 0; col <5; col++) {
                GamePiece *newPiece;
                if (row == col && !(row % 2)) {
                    newPiece = [[Musketeer alloc] init];
                    [musketeers addObject:newPiece];
                }
                else {
                    newPiece = [[CardinalMan alloc] init];
                    [self.cardinalsMen addObject:newPiece];
                }
                GameSquare *newSquare = [[GameSquare alloc]initWithRow:row andColumn:col];
                [newSquare setGamePiece:newPiece];
                [column addObject: newSquare];
            }
            [rows addObject:column];
        }
        _boardArrays = [rows copy];
        self.musketeers = [musketeers copy];
        self.cursorSquare = _boardArrays[0][0];
    }
    return _boardArrays;
}

- (NSString*)boardStringWithHighlight: (BOOL)shouldHighlight
{
    [self.cursorSquare setIsHighlighted:shouldHighlight];
    NSMutableString *newBoardString = [NSMutableString string];
    for (NSArray *row in self.boardArrays) {
        for (GameSquare *square in row) {
            [newBoardString appendString:[square squareString]];
        }
        [newBoardString appendString:@"\n"];
    }
    
    return [newBoardString copy];
}

- (void)moveHighlightToSquare:(GameSquare*)gameSquare
{
    GameSquare * oldSquare = self.cursorSquare;
    self.cursorSquare = gameSquare;
    if (oldSquare != self.cursorSquare) {
        [oldSquare setIsHighlighted:NO];
    }
}

- (void) moveCursor:(char)moveChar
{
    
    NSInteger newRow = self.cursorSquare.row;
    NSInteger newColumn = self.cursorSquare.column;
    switch (moveChar) {
        case 'w':
            if (newRow > 0) {
                newRow--;
            }
            break;
        case 's':
            if (newRow < 4) {
                newRow++;
            }
            break;
        case 'a':
            if (newColumn > 0) {
                newColumn--;
            }
            break;
        case 'd':
            if (newColumn < 4) {
                newColumn++;
            }
            break;
        default:
            break;
    }
    [self moveHighlightToSquare:self.boardArrays[newRow][newColumn]];
}
- (GameSquare*)getCursorUpSquare
{
    return [self getUpSquareForSquare:self.cursorSquare];
}
- (GameSquare*)getCursorRightSquare
{
    return [self getRightSquareForSquare:self.cursorSquare];
}
- (GameSquare*)getCursorLeftSquare
{
    return [self getLeftSquareForSquare:self.cursorSquare];
}
- (GameSquare*)getCursorDownSquare
{
    return [self getDownSquareForSquare:self.cursorSquare];
}

- (GameSquare*)getUpSquareForSquare:(GameSquare*)square
{
    if (square.row == 0) {
        return nil;
    }
    return self.boardArrays[square.row-1][square.column];
}
- (GameSquare*)getRightSquareForSquare:(GameSquare*)square
{
    if (square.row == 4) {
        return nil;
    }
    return self.boardArrays[square.row+1][square.column];
}
- (GameSquare*)getLeftSquareForSquare:(GameSquare*)square
{
    if (square.column == 0) {
        return nil;
    }
    return self.boardArrays[square.row][square.column-1];
}
- (GameSquare*)getDownSquareForSquare:(GameSquare*)square
{
    if (square.column == 4) {
        return nil;
    }
    return self.boardArrays[square.row][square.column+1];
}

- (void)moveCursorSquaretoSquare:(GameSquare*)destination
{
    if ([destination.gamePiece isCardinal]) {
         [self.cardinalsMen removeObject:destination.gamePiece];
    }
    destination.gamePiece = self.cursorSquare.gamePiece;
    self.cursorSquare.gamePiece = nil;
    [self moveHighlightToSquare:destination];
}

- (char)cursorPieceChar
{
    return self.cursorSquare.gamePiece.pieceChar;
}

- (BOOL) musketeersLinedUp
{
    NSMutableSet *rowValues = [NSMutableSet set];
    NSMutableSet *columnValues = [NSMutableSet set];
    for (Musketeer *musketeer in self.musketeers) {
        [rowValues addObject:@(musketeer.gameSquare.row)];
        [columnValues addObject:@(musketeer.gameSquare.column)];
    }
    return rowValues.count == 1 || columnValues.count == 1;
    
}

- (BOOL) musketeersCanMove
{
    for (Musketeer *musketeer in self.musketeers) {
        GameSquare *square = musketeer.gameSquare;
        if ([[self getUpSquareForSquare:square] hasCardinal]) {
            return YES;
        }
        if ([[self getDownSquareForSquare:square] hasCardinal]) {
            return YES;
        }
        if ([[self getLeftSquareForSquare:square] hasCardinal]) {
            return YES;
        }
        if ([[self getRightSquareForSquare:square] hasCardinal]) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL) cardinalsMenCanMove
{
    for (CardinalMan *cardinalMan in self.cardinalsMen) {
        GameSquare *square = cardinalMan.gameSquare;
        if ([[self getUpSquareForSquare:square] hasNoPiece]) {
            return YES;
        }
        if ([[self getDownSquareForSquare:square] hasNoPiece]) {
            return YES;
        }
        if ([[self getLeftSquareForSquare:square] hasNoPiece]) {
            return YES;
        }
        if ([[self getRightSquareForSquare:square] hasNoPiece]) {
            return YES;
        }
    }
    
    return NO;
}

@end
