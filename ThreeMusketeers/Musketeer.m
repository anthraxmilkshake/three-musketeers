//
//  Musketeer.m
//  ThreeMusketeers
//
//  Created by Mark Griffith on 3/28/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "Musketeer.h"

char const MusketeerChar = 'M';

@implementation Musketeer

-(char) pieceChar
{
    return MusketeerChar;
}

- (BOOL) isMusketeer
{
    return YES;
}

@end
