//
//  main.m
//  ThreeMusketeers
//
//  Created by Mark Griffith on 3/28/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThreeMusketeerGame.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // create and start game
        ThreeMusketeerGame * game = [[ThreeMusketeerGame alloc] init];
        [game start];
        
    }
    return 0;
}
