//
//  ThreeMusketeerGame.h
//  ThreeMusketeers
//
//  Created by Mark Griffith on 3/28/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString * const GameExitNotificationMessage;

@interface ThreeMusketeerGame : NSObject

- (void) start;

@end
