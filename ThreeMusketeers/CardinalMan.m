//
//  CardinalMan.m
//  ThreeMusketeers
//
//  Created by Mark Griffith on 3/28/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "CardinalMan.h"

char const CardinalChar = 'C';

@implementation CardinalMan

-(char) pieceChar
{
    return CardinalChar;
}

- (BOOL) isCardinal
{
    return YES;
}
@end
