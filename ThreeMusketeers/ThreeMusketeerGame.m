//
//  ThreeMusketeerGame.m
//  ThreeMusketeers
//
//  Created by Mark Griffith on 3/28/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "ThreeMusketeerGame.h"
#import "GameBoard.h"
#import <termios.h>
#import "GameSquare.h"
#import "PlayerOneSelectState.h"
#import "PlayerOneMoveState.h"
#import "PlayerTwoSelectState.h"
#import "PlayerTwoMoveState.h"
#import "GameStateMachine.h"


NSString * const GameExitNotificationMessage = @"GameExitNotificationMessage";

@interface ThreeMusketeerGame()

@property (nonatomic, strong) GameBoard * board;
@property (nonatomic, assign) BOOL gameRunning;
@property (nonatomic, strong) NSTimer *mainTimer;
@property (nonatomic, assign) BOOL highlightToggle;
@property (nonatomic, strong) GameStateMachine *gameStateMachine;

@end

@implementation ThreeMusketeerGame

- (instancetype)init
{
    self = [super init];
    if (self) {
        _board = [[GameBoard alloc]init];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onGameExitNotification:)
                                                     name:GameExitNotificationMessage
                                                   object:nil];
        _gameRunning = YES;
    }
    return self;
}

- (void)onGameExitNotification: (NSNotification*)notification
{
    if ([[notification name]isEqualToString:GameExitNotificationMessage]) {
            exit(0);
    }

}

- (void) start
{
    //Don't need to press enter, shamelessly taken from stack overflow
    struct termios terminal_info;
    tcgetattr(STDIN_FILENO, &terminal_info);
    terminal_info.c_lflag &= ~ICANON;
    tcsetattr(STDIN_FILENO, TCSANOW, &terminal_info);
    self.gameStateMachine = [[GameStateMachine alloc] initWithBoard:self.board];
    
    //Initial Update
    [self gameUpdate];
   
    while (self.gameRunning) {
       [self handleInput];
    }
    
}

- (void)gameUpdate
{
    [self refreshScreen];
    
    //Because NSTimers don't like background threads
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    NSTimeInterval delay = .25;
    dispatch_time_t dispatchTime = dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC);
    dispatch_after(dispatchTime, backgroundQueue, ^(void){
        [self gameUpdate];
    });
}

- (void) refreshScreen
{
    [self refreshScreenForceHighlight:NO];
}

- (void) refreshScreenForceHighlight:(BOOL) forceHighlight
{
    for (int i = 0; i <100; i++){
        printf("\n");
    }
    
    printf("THREE MUSKETEERS (w,a,s,d to Move; z to Select/Deselect)\n");
    printf("%s",[[self.board boardStringWithHighlight:self.highlightToggle || forceHighlight] UTF8String]);
    printf("%s", [[self.gameStateMachine message] UTF8String]);
    
    self.highlightToggle = !self.highlightToggle;
}

- (void) handleInput
{
    char input = getchar();
    [self.gameStateMachine handleInputChar:input];
    [self refreshScreenForceHighlight:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
