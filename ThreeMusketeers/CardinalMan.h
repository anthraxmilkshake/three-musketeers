//
//  CardinalMan.h
//  ThreeMusketeers
//
//  Created by Mark Griffith on 3/28/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "GamePiece.h"
extern char const CardinalChar;

@interface CardinalMan : GamePiece

@end
