//
//  GameSquare.m
//  ThreeMusketeers
//
//  Created by Mark Griffith on 4/2/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "GameSquare.h"
#import "GamePiece.h"

@interface GameSquare()

@property (nonatomic, assign) NSInteger row;
@property (nonatomic, assign) NSInteger column;


@end

@implementation GameSquare

- (instancetype)initWithRow:(NSInteger)row andColumn:(NSInteger)column
{
    self = [self init];
    if (self) {
        _row = row;
        _column = column;
    }
    return self;
}

- (NSString*)squareString
{
    if (self.isHighlighted) {
        return @"▓";
    }
    if (self.gamePiece) {
        return [NSString stringWithFormat:@"%c",[self.gamePiece pieceChar]];
    }
    return @"░";
}
- (void)setGamePiece:(GamePiece *)gamePiece
{
    _gamePiece = gamePiece;
    _gamePiece.gameSquare = self;
}

- (BOOL)hasCardinal
{
    return [self.gamePiece isCardinal];
}

- (BOOL)hasMusketeer
{
    return [self.gamePiece isMusketeer];
}

- (BOOL)hasNoPiece
{
    return !self.gamePiece;
}

@end
