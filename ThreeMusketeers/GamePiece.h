//
//  GamePiece.h
//  ThreeMusketeers
//
//  Created by Mark Griffith on 3/28/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GameSquare;

@interface GamePiece : NSObject

@property (nonatomic, readonly, assign) char pieceChar;
@property (nonatomic, weak) GameSquare *gameSquare;

- (BOOL)isMusketeer;
- (BOOL)isCardinal;

@end
