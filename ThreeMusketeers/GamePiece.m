//
//  GamePiece.m
//  ThreeMusketeers
//
//  Created by Mark Griffith on 3/28/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "GamePiece.h"

@implementation GamePiece

- (BOOL)isCardinal
{
    return NO;
}

- (BOOL)isMusketeer
{
    return NO;
}

@end
