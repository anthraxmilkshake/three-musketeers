//
//  GameSquare.h
//  ThreeMusketeers
//
//  Created by Mark Griffith on 4/2/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import <Foundation/Foundation.h>
@class GamePiece;

@interface GameSquare : NSObject

@property (nonatomic, strong) GamePiece *gamePiece;
@property (nonatomic, readonly, copy) NSString *squareString;
@property (nonatomic, assign, readonly) NSInteger row;
@property (nonatomic, assign, readonly) NSInteger column;
@property (nonatomic, assign) BOOL isHighlighted;
- (BOOL) hasCardinal;
- (BOOL) hasMusketeer;
- (BOOL) hasNoPiece;


- (instancetype)initWithRow:(NSInteger)row andColumn:(NSInteger)column;

@end
