//
//  GameStateMachine.m
//  ThreeMusketeers
//
//  Created by Mark Griffith on 4/3/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "GameStateMachine.h"
#import "PlayerOneMoveState.h"
#import "PlayerTwoMoveState.h"
#import "PlayerOneSelectState.h"
#import "PlayerTwoSelectState.h"
#import "GameOverGameState.h"

@interface GameStateMachine()

@property (nonatomic, strong) GameState *currentState;
@property (nonatomic, strong) PlayerOneSelectState *playerOneSelectState;
@property (nonatomic, strong) PlayerTwoSelectState *playerTwoSelectState;
@property (nonatomic, strong) PlayerOneMoveState *playerOneMoveState;
@property (nonatomic, strong) PlayerTwoMoveState *playerTwoMoveState;
@property (nonatomic, strong) GameOverGameState *gameOverGameState;
@property (nonatomic, strong) GameBoard *board;
@property (nonatomic, assign) BOOL playerOneWon;
@property (nonatomic, assign) BOOL playerTwoWon;
@property (nonatomic, assign) BOOL isStalemate;

@end

@implementation GameStateMachine

- (instancetype)initWithBoard:(GameBoard*)board
{
    self = [self init];
    if (self) {
        _board = board;
        _playerOneSelectState = [[PlayerOneSelectState alloc]initWithGameBoard:board];
        _playerOneSelectState.delegate = self;
        _playerTwoSelectState = [[PlayerTwoSelectState alloc]initWithGameBoard:board];
        _playerTwoSelectState.delegate = self;
        _playerOneMoveState = [[PlayerOneMoveState alloc]initWithGameBoard:board];
        _playerOneMoveState.delegate = self;
        _playerTwoMoveState = [[PlayerTwoMoveState alloc]initWithGameBoard:board];
        _playerTwoMoveState.delegate = self;
        _gameOverGameState = [[GameOverGameState alloc]initWithGameBoard:board];
        _gameOverGameState.delegate = self;
        
        [self switchToGameState:GameStateEnum_PlayerOneSelect];
    }
    return self;
}

- (void)handleInputChar:(char) inputChar
{
    [self.currentState handleInputChar:inputChar];
}

- (void)switchToGameState:(GameStateEnum)gameState
{
    switch (gameState) {
        case GameStateEnum_PlayerOneMove:
            self.currentState = self.playerOneMoveState;
            break;
        case GameStateEnum_PlayerTwoMove:
            self.currentState = self.playerTwoMoveState;
            break;
        case GameStateEnum_PlayerOneSelect:
            self.currentState = self.playerOneSelectState;
            break;
        case GameStateEnum_PlayerTwoSelect:
            self.currentState = self.playerTwoSelectState;
            break;
        case GameStateEnum_GameOver:
            self.currentState = self.gameOverGameState;
        default:
            break;
    }
}

- (NSString*)message
{
    return [self.currentState message];
}

- (BOOL)isGameOver
{
    if (self.playerOneWon || self.playerTwoWon || self.isStalemate) {
        return YES;
    }
    if (![self.board musketeersCanMove]) {
        if ([self.board musketeersLinedUp]) {
            self.playerTwoWon = YES;
        }
        else {
            self.playerOneWon = YES;
        }
    }
    
    return self.playerOneWon || self.playerTwoWon || self.isStalemate;
}

- (void)playerOneMoved
{
    self.isStalemate = ![self.board cardinalsMenCanMove];
    
    if ([self isGameOver]) {
        [self switchToGameState:GameStateEnum_GameOver];
    } else{
        [self switchToGameState:GameStateEnum_PlayerTwoSelect];
    }
}
- (void)playerOneSelected
{
    [self switchToGameState:GameStateEnum_PlayerOneMove];
}
- (void)playerTwoMoved
{
    if ([self isGameOver]) {
        [self switchToGameState:GameStateEnum_GameOver];
    } else{
        [self switchToGameState:GameStateEnum_PlayerOneSelect];
    }
    
}
- (void)playerTwoSelected
{
    [self switchToGameState:GameStateEnum_PlayerTwoMove];
}
- (NSInteger) winningPlayer
{
    if (self.playerOneWon) {
        return 1;
    }
    if (self.playerTwoWon) {
        return 2;
    }
    return 0;
}

@end
