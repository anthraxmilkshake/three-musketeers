//
//  GameStateMachine.h
//  ThreeMusketeers
//
//  Created by Mark Griffith on 4/3/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameState.h"

@interface GameStateMachine : NSObject <GameStateDelegate>

- (instancetype)initWithBoard:(GameBoard*)board;
- (void)handleInputChar:(char) inputChar;
- (NSString*) message;

@end
