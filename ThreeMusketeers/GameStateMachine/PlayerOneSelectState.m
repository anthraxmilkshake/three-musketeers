//
//  PlayerOneSelectState.m
//  ThreeMusketeers
//
//  Created by Mark Griffith on 4/2/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "PlayerOneSelectState.h"
#import "GameSquare.h"

@implementation PlayerOneSelectState

- (NSString*)message
{
    return @"PLAYER 1: Select a Piece\n";
}

- (void)handleInputChar:(char)inputChar
{
    switch (inputChar) {
        case GameMoveEnum_Up:
        case GameMoveEnum_Down:
        case GameMoveEnum_Left:
        case GameMoveEnum_Right:
            [self.gameBoard moveCursor:inputChar];
            break;
        case GameMoveEnum_Select:
            if ([self.gameBoard.cursorSquare hasMusketeer]) {
                [self.delegate playerOneSelected];
            }
            break;
        default:
            break;
    }
}

@end
