//
//  GameState.m
//  ThreeMusketeers
//
//  Created by Mark Griffith on 4/2/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "GameState.h"

@implementation GameState

- (instancetype)initWithGameBoard: (GameBoard*)gameBoard
{
    self = [self init];
    if (self) {
        _gameBoard = gameBoard;
    }
    return self;
}

- (void)handleInputChar:(char)inputChar
{
    //Handle in subclass
}


@end
