//
//  PlayerTwoSelectState.m
//  ThreeMusketeers
//
//  Created by Mark Griffith on 4/2/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "PlayerTwoSelectState.h"
#import "GameSquare.h"

@implementation PlayerTwoSelectState

- (NSString*)message
{
    return @"PLAYER 2: Select a Piece\n";
}


- (void)handleInputChar:(char)inputChar
{
    switch (inputChar) {
        case GameMoveEnum_Up:
        case GameMoveEnum_Down:
        case GameMoveEnum_Left:
        case GameMoveEnum_Right:
            [self.gameBoard moveCursor:inputChar];
            break;
        case GameMoveEnum_Select:
            if ([self.gameBoard.cursorSquare hasCardinal]) {
                [self.delegate playerTwoSelected];
            }
            break;
        default:
            break;
    }
}

@end
