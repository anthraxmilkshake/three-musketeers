//
//  GameOverGameState.h
//  ThreeMusketeers
//
//  Created by Mark Griffith on 4/3/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "GameState.h"

@interface GameOverGameState : GameState

@end
