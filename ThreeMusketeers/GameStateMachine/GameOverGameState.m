//
//  GameOverGameState.m
//  ThreeMusketeers
//
//  Created by Mark Griffith on 4/3/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "GameOverGameState.h"
#import "ThreeMusketeerGame.h"
@implementation GameOverGameState

- (NSString*)message
{
    NSInteger winningPlayer = [self.delegate winningPlayer];
    if (!winningPlayer) {
        return @"STALEMATE: Nobody wins. Press x to exit.\n";
    }
    return [NSString stringWithFormat:@"Game Over! Player %ld Wins! Press x to exit.\n", [self.delegate winningPlayer]];
}

- (void)handleInputChar:(char)inputChar
{
    if (inputChar == GameMoveEnum_Exit) {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:GameExitNotificationMessage
         object:self];
    }
}

@end
