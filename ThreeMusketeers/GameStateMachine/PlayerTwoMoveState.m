//
//  PlayerTwoMoveState.m
//  ThreeMusketeers
//
//  Created by Mark Griffith on 4/2/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import "PlayerTwoMoveState.h"
#import "GameSquare.h"

@implementation PlayerTwoMoveState

- (NSString*)message
{
    return @"PLAYER 2: Move Selected Piece or press z to deselect\n";
}

- (void)handleInputChar:(char)inputChar
{
    GameSquare * newSquare;
    switch (inputChar) {
        case GameMoveEnum_Up:
            newSquare = [self.gameBoard getCursorUpSquare];
            break;
        case GameMoveEnum_Down:
            newSquare = [self.gameBoard getCursorRightSquare];
            break;
        case GameMoveEnum_Left:
            newSquare = [self.gameBoard getCursorLeftSquare];
            break;
        case GameMoveEnum_Right:
            newSquare = [self.gameBoard getCursorDownSquare];
            break;
        case GameMoveEnum_Select:
            [self.delegate switchToGameState:GameStateEnum_PlayerTwoSelect];
            return;
        default:
            break;
    }
    if ([newSquare hasNoPiece]) {
        [self.gameBoard moveCursorSquaretoSquare:newSquare];
        [self.delegate playerTwoMoved];
    }
}

@end
