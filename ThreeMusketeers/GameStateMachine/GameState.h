//
//  GameState.h
//  ThreeMusketeers
//
//  Created by Mark Griffith on 4/2/18.
//  Copyright © 2018 FlipYourBits. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameBoard.h"
#import "Musketeer.h"
#import "CardinalMan.h"

typedef NS_ENUM(NSInteger, GameStateEnum) {
    GameStateEnum_PlayerOneSelect,
    GameStateEnum_PlayerOneMove,
    GameStateEnum_PlayerTwoSelect,
    GameStateEnum_PlayerTwoMove,
    GameStateEnum_GameOver
};

typedef NS_ENUM(char, GameMoveEnum) {
    GameMoveEnum_Up = 'w',
    GameMoveEnum_Down = 's',
    GameMoveEnum_Left = 'a',
    GameMoveEnum_Right = 'd',
    GameMoveEnum_Select = 'z',
    GameMoveEnum_Exit = 'x'
};

@protocol GameStateDelegate

- (void)switchToGameState:(GameStateEnum)gameState;
- (void)playerOneMoved;
- (void)playerOneSelected;
- (void)playerTwoMoved;
- (void)playerTwoSelected;
- (NSInteger)winningPlayer;

@end

@interface GameState : NSObject

@property (nonatomic, strong, readonly) GameBoard *gameBoard;
@property (nonatomic, weak)id<GameStateDelegate> delegate;
@property (nonatomic, readonly, copy) NSString *message;

- (void)handleInputChar:(char) inputChar;
- (instancetype)initWithGameBoard: (GameBoard*)gameBoard;

@end
